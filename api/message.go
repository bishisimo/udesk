// @Author：bishisimo  2021/4/2 下午3:26
// @Description：提供发送消息相关功能
package api

import (
	"gitee.com/bishisimo/udesk/option"
)

/**
 * @Description: 发送消息的底层api，可提供完整功能
 * @receiver d
 * @param messageOption 消息配置
 * @return *option.MessageResp
 * @return error
 */
func (d UDesk) SendMessageBase(messageOption *option.MessageOption) (*option.MessageResp, error) {
	url := d.GetUDeskUrl("message")
	r := new(option.MessageResp)
	_, err := d.Request().SetBody(messageOption).SetResult(r).Post(url)
	return r, err
}

/**
 * @Description: 发送人工客服问题
 * @receiver d
 * @param imSubSessionId 会话id
 * @param messageId 用来匹配回调问题与答案
 * @param messageType 消息内容类型MessageTypeEnum
 * @param data
 * @return *option.MessageResp
 * @return error
 */
func (d UDesk) SendMessageAgent(imSubSessionId int, messageId string, messageType string, data *option.MessageData) (*option.MessageResp, error) {
	message := &option.MessageOption{
		CustomerToken:  d.CustomerToken,
		ImSubSessionId: imSubSessionId,
		MessageId:      messageId,
		Type:           messageType,
		Data:           *data,
	}
	return d.SendMessageBase(message)
}

/**
 * @Description: 发送urobot选中的问题
 * @receiver d
 * @param content 点击问题的内容
 * @return *option.MessageResp
 * @return error
 */
func (d *UDesk) SendMessageURobot(content string) (*option.MessageResp, error) {
	message := &option.MessageOption{
		CustomerToken:  d.CustomerToken,
		ImSubSessionId: option.SessionTypeEnum.URobotType,
		Type:           option.MessageTypeEnum.Message,
		Data: option.MessageData{
			Content: content,
		},
		RobotId:         d.RobotId,
		SceneId:         d.SceneId,
		URobotSessionId: d.SessionId,
		AgentId:         0,
		GroupId:         0,
	}
	return d.SendMessageBase(message)
}

/**
 * @Description: 关闭消息通知
 * @receiver d
 * @return *option.MessageResp
 * @return error
 */
func (d UDesk) SendCloseMessage() (*option.MessageResp, error) {
	message := &option.MessageOption{
		CustomerToken: d.CustomerToken,
		Type:          option.MessageTypeEnum.Close,
		Data: option.MessageData{
			Content: "session close",
		},
	}
	return d.SendMessageBase(message)
}

/**
 * @Description: 暂时无法使用
 * @receiver d
 * @param messageId
 * @return *option.MessageResp
 * @return error
 */
func (d UDesk) FlowMessage(messageId int) (*option.MessageResp, error) {
	url := d.GetUDeskUrl("flow")
	body := &option.FlowOption{
		CustomerToken:  d.CustomerToken,
		ImSubSessionId: d.SessionId,
		FlowId:         messageId,
		RobotId:        d.RobotId,
		SceneId:        d.SceneId,
	}
	r := new(option.MessageResp)
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	return r, err
}

/**
 * @Description: 暂时无法使用
 * @receiver d
 * @param messageId
 * @return *option.MessageResp
 * @return error
 */
func (d UDesk) HitMessage(messageId int) (*option.MessageResp, error) {
	url := d.GetUDeskUrl("hit")
	body := &option.HitOption{
		CustomerToken:  d.CustomerToken,
		ImSubSessionId: d.SessionId,
		MessageId:      messageId,
		RobotId:        d.RobotId,
		Question:       d.Question,
		QueryType:      d.QueryType,
	}
	r := new(option.MessageResp)
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	return r, err
}
