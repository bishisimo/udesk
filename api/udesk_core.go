// @Author：bishisimo   0:58
// @Description
package api

import (
	"fmt"
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"github.com/go-resty/resty/v2"
	"github.com/spf13/viper"
	"time"
)

/**
 * @Description: 储存固定的UDesk配置，是创建UDesk请求的固定参数
 */
type uDeskCore struct {
	*option.UDeskOption
	UDeskToken string `json:"udesk_token"` //账户token
	ChanSize   int    `json:"chan_size"`   //控制创建的消息通道大小
}

/**
 * @Description: 获取UDesk路由，由配置文件获取
 * @receiver c
 * @param name 路由对应名称
 * @return string
 */
func (c *uDeskCore) GetUDeskUrl(name string) string {
	return c.Domain + c.Api + viper.GetString(option.UDeskUrlPrefix+name)
}

/**
 * @Description: 生成默认的签名请求参数用以发起UDesk资源请求
 * @receiver c
 * @return map[string]string
 */
func (c *uDeskCore) newSignMap() map[string]string {
	timestamp := time.Now().Unix()
	nonce := utils.GenerateUUID()
	sign := utils.UDeskSign(c.UDeskToken, timestamp, nonce)
	return map[string]string{
		"email":        c.Email,
		"token":        c.UDeskToken,
		"timestamp":    fmt.Sprintf("%v", timestamp),
		"nonce":        nonce,
		"sign_version": "v2",
		"sign":         sign,
	}
}

/**
 * @Description: 获取由默认签名参数的Http请求器
 * @receiver c
 * @return *resty.Request
 */
func (c uDeskCore) Request() *resty.Request {
	return resty.New().R().SetQueryParams(c.newSignMap())
}
