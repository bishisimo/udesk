// @Author：bishisimo  2021/4/1 上午4:38
// @Description：
package api

import (
	"gitee.com/bishisimo/udesk/option"
)

/**
 * @Description: 每个用户的独立通信结构体
 */
type UDesk struct {
	*uDeskCore    `json:"udesk_core"`        //ctl静态信息
	CustomerToken string                     `json:"customer_token"` //用户唯一标识
	HeatTime      int64                      `json:"heat_time"`      //心跳时间，用来标记超时会话
	ClientChan    chan *option.MessageOption `json:"client_chan"`    //消息chan，接受客户端消息
	UDeskChan     chan interface{}           `json:"udesk_chan"`     //消息chan，接受udesk消息
	*SessionInfo  `json:"session_info"`
}

func (d *UDesk) initChan() {
	if d.ClientChan == nil {
		d.ClientChan = make(chan *option.MessageOption, d.ChanSize)
	}
	if d.UDeskChan == nil {
		d.UDeskChan = make(chan interface{}, d.ChanSize)
	}
}

/**
 * @Description: 当前用户最近会话信息用以后续问答反馈
 */
type SessionInfo struct {
	SessionType string `json:"session_type"`
	//MessageId       int `json:"messgae_id"`        //	问题答案所在的消息id
	//ImSubSessionId  int `json:"im_sub_session_id"` //	会话id
	RobotId   int `json:"robot_id"`   //	udesk机器人id
	SceneId   int `json:"scene_id"`   //	udesk机器人对应场景id
	SessionId int `json:"session_id"` //	Udesk机器人对应的会话id
	//URobotLogId     int `json:"urobot_log_id"`//
	AgentId int `json:"agent_id"` //		指定分配的客服id 配合im_sub_session_id=-2使用
	GroupId int `json:"group_id"` //		指定分配的客服组id 配合im_sub_session_id=-2使用
	//robot
	QueryType int `json:"query_type"`
	Question  int `json:"question"`
}

/**
 * @Description: 通过个体信息构建UDesk通信对象
 * @param uDeskInfo
 * @param customerToken
 * @return *UDesk
 */
func newUDesk(uDeskInfo *uDeskCore, customerToken string) *UDesk {
	u := &UDesk{
		uDeskCore:     uDeskInfo,
		CustomerToken: customerToken,
		SessionInfo:   new(SessionInfo),
	}
	return u
}
