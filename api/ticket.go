// @Author：bishisimo  2021/4/7 下午3:26
// @Description：提供工单相关功能
package api

import (
	"bytes"
	"fmt"
	"gitee.com/bishisimo/udesk/option"
	"io/ioutil"
	"strconv"
)

/**
 * @Description: 创建工单
 * @receiver c
 * @param createOption
 * @return *option.TicketCreateResp
 * @return error
 */
func (c uDeskCore) CreateTicket(createOption *option.TicketCreateOption) (*option.TicketCreateResp, error) {
	url := c.GetUDeskUrl("ticket")
	r := new(option.TicketCreateResp)
	createOption.CreatorEmail = ""
	_, err := c.Request().SetBody(createOption).SetResult(r).Post(url)
	return r, err
}

/**
 * @Description: 更新工单
 * @receiver c
 * @param id
 * @param updateOption
 * @return *option.TicketUpdateResp
 * @return error
 */
func (c uDeskCore) UpdateTicket(id int, updateOption *option.TicketUpdateOption) (*option.TicketUpdateResp, error) {
	url := c.GetUDeskUrl("ticket") + fmt.Sprintf("/%v", id)
	r := new(option.TicketCreateResp)
	_, err := c.Request().SetBody(updateOption).SetResult(r).Put(url)
	return r, err
}

/**
 * @Description: 通过工单id获取工单信息
 * @receiver c
 * @param id 通过工单id获取工单信息
 * @return *option.TicketCreateResp
 * @return error
 */
func (c uDeskCore) GetTicketDetailById(id int) (*option.TicketCreateResp, error) {
	url := c.GetUDeskUrl("ticket_detail")
	r := new(option.TicketCreateResp)
	_, err := c.Request().SetQueryParam("id", fmt.Sprintf("%v", id)).SetResult(r).Get(url)
	return r, err
}

/**
 * @Description: 通过工单编号获取工单信息
 * @receiver c
 * @param num 工单编号
 * @return *option.TicketCreateResp
 * @return error
 */
func (c uDeskCore) GetTicketDetailByNum(num string) (*option.TicketCreateResp, error) {
	url := c.GetUDeskUrl("ticket_detail")
	r := new(option.TicketCreateResp)
	_, err := c.Request().SetQueryParam("num", num).SetResult(r).Get(url)
	return r, err
}

/**
 * @Description: 通过工单id删除工单
 * @receiver c
 * @param id
 * @return *option.TicketCreateResp
 * @return error
 */
func (c uDeskCore) DeleteTicketById(id int) (*option.TicketCreateResp, error) {
	url := c.GetUDeskUrl("ticket_delete")
	r := new(option.TicketCreateResp)
	_, err := c.Request().SetQueryParam("id", fmt.Sprintf("%v", id)).SetResult(r).Delete(url)
	return r, err
}

/**
 * @Description: 通过工单num删除工单
 * @receiver c
 * @param num
 * @return *option.TicketCreateResp
 * @return error
 */
func (c uDeskCore) DeleteTicketByNum(num string) (*option.TicketCreateResp, error) {
	url := c.GetUDeskUrl("ticket_delete")
	r := new(option.TicketCreateResp)
	_, err := c.Request().SetQueryParam("num", num).SetResult(r).Delete(url)
	return r, err
}

/**
 * @Description: 流式传输上传附件到UDesk
 * @receiver c
 * @param ticketId 工单创建后的id
 * @param fileName 文件名(非本地路径)
 * @param filePath 文件暂存路径
 * @return *option.TicketAttachResp
 * @return error
 */
func (c uDeskCore) TicketAttach(ticketId int, fileName, filePath string) (*option.TicketAttachResp, error) {
	url := c.GetUDeskUrl("ticket_attach")
	r := new(option.TicketAttachResp)
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	_, err = c.Request().SetHeader("Content-type", "application/octet-stream").SetQueryParams(map[string]string{
		"ticket_id": strconv.Itoa(ticketId),
		"file_name": fileName,
	}).SetBody(bytes.NewReader(data)).SetResult(r).Post(url)
	return r, err
}
