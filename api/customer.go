// @Author：bishisimo  2021/4/9 下午2:32
// @Description：客服相关api
package api

import (
	"gitee.com/bishisimo/udesk/option"
)

/**
 * @Description: 获取客户信息
 * @receiver c
 * @param idType 客户标准类型
 * @param idContent 客户表示对应的值
 * @return *option.CustomerQueryResp 客户查询响应
 * @return error
 */
func (c *uDeskCore) GetCustomerInfo(idType string, idContent string) (*option.CustomerQueryResp, error) {
	url := c.GetUDeskUrl("customer_info")
	r := new(option.CustomerQueryResp)
	_, err := c.Request().SetQueryParams(
		map[string]string{
			"type":    idType,
			"content": idContent,
		}).SetResult(r).Get(url)
	//utils.DebugRespRaw("GetCustomerInfo", resp)
	return r, err
}

/**
 * @Description: 创建客户
 * @receiver d
 * @param customerEmail 客户邮箱信息
 * @param customerName 客户名字信息
 * @return *option.CustomerCreateResp 客户创建响应
 * @return error
 */
func (d *UDesk) CreateCustomer(customerEmail, customerName string) (*option.CustomerCreateResp, error) {
	url := d.GetUDeskUrl("customer_create")
	r := new(option.CustomerCreateResp)
	body := &option.CustomerCreateOption{
		CustomerInfo: option.CustomerInfo{
			Email:        customerEmail,
			OpenApiToken: d.CustomerToken,
			NickName:     customerName,
		},
	}
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	return r, err
}

/**
 * @Description: 获取客户信息,如果不存在就创建
 * @receiver d
 * @param customerEmail 客户邮箱信息
 * @param customerName 客户名字信息
 * @return *option.CustomerCreateResp 客户创建响应
 * @return error
 */
func (d *UDesk) GetOrCreateCustomer(customerEmail, customerName string) (*option.CustomerCreateResp, error) {
	customer, err := d.GetCustomerInfo(option.CustomerTypeEnum.Token, d.CustomerToken)
	if err != nil || customer.Code != option.SuccessCode || customer.OpenApiToken != d.CustomerToken {
		customer, err = d.GetCustomerInfo(option.CustomerTypeEnum.Email, customerEmail)
		if err != nil || customer.Code != option.SuccessCode || customer.Email != customerEmail {
			return d.CreateCustomer(customerEmail, customerName)
		}
	}
	return customer, err
}

/**
 * @Description: 更新客户信息
 * @receiver d
 * @param otherEmail 客户非主邮箱信息
 * @return *option.CustomerUpdateResp 客户更新响应
 * @return error
 */
func (d *UDesk) UpdateCustomer(otherEmail string) (*option.CustomerUpdateResp, error) {
	url := d.GetUDeskUrl("customer_update")
	r := new(option.CustomerUpdateResp)
	body := map[string]interface{}{
		"customer": map[string]string{
			"open_api_token": d.CustomerToken,
			"email":          otherEmail,
		},
		"other_emails": [][]interface{}{{nil, otherEmail}},
	}
	_, err := d.Request().SetQueryParams(map[string]string{
		"type":    option.CustomerTypeEnum.Token,
		"content": d.CustomerToken,
	}).SetBody(body).SetResult(r).Put(url)
	return r, err
}

/**
 * @Description: 删除客户
 * @receiver d
 * @return *option.CustomerDeleteResp 客户删除响应
 * @return error
 */
func (d UDesk) DeleteCustomer() (*option.CustomerDeleteResp, error) {
	url := d.GetUDeskUrl("customer_delete")
	r := new(option.CustomerDeleteResp)
	_, err := d.Request().SetQueryParams(map[string]string{
		"type":    option.CustomerTypeEnum.Token,
		"content": d.CustomerToken,
	}).SetResult(r).Delete(url)
	return r, err
}
