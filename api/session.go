// @Author：bishisimo  2021/4/1 下午2:10
// @Description：
package api

import (
	"fmt"
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"time"
)

/**
 * @Description: 获取agent类型的会话
 * @receiver d
 * @return *option.AgentSessionResp
 * @return error
 */
func (d *UDesk) GetAgentSession() (*option.AgentSessionResp, error) {
	return d.CreateAgentSession()
}

/**
 * @Description: 创建客服会话
 * @receiver d
 * @return *option.AgentSessionResp
 * @return error
 */
func (d *UDesk) CreateAgentSession() (*option.AgentSessionResp, error) {
	url := d.GetUDeskUrl("session")
	r := new(option.AgentSessionResp)
	body := option.NewAgentSessionOption(d.CustomerToken)
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	if err != nil {
		return nil, err
	}
	d.SessionId = r.AssignInfo.ImSubSessionId
	d.SessionType = r.AssignType
	d.HeatTime = time.Now().Unix()
	d.initChan()
	return r, err
}

/**
 * @Description: 获取urobot类型的会话
 * @receiver d
 * @param robotId
 * @param sceneId
 * @return *option.URobotSessionResp
 * @return error
 */
func (d *UDesk) GetURobotSession(robotId int, sceneId int) (*option.URobotSessionResp, error) {
	return d.CreateURobotSession(robotId, sceneId)
}

/**
 * @Description: 创建urobot类型的会话，用来实现问题列表交互
 * @receiver d
 * @param robotId 机器人id
 * @param sceneId 场景id
 * @return *option.URobotSessionResp
 * @return error
 */
func (d *UDesk) CreateURobotSession(robotId int, sceneId int) (*option.URobotSessionResp, error) {
	if robotId == 0 && sceneId == 0 {
		uRobotRespond, err := d.GetURobots()
		utils.LogErr(err)
		uRobot := uRobotRespond.URobotList[0]
		scene := uRobot.SceneList[0]
		robotId = uRobot.Id
		sceneId = scene.Id
	}
	url := d.GetUDeskUrl("session")
	r := new(option.URobotSessionResp)
	body := option.NewURobotSessionOption(d.CustomerToken, "", robotId, sceneId)
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	d.RobotId = robotId
	d.SceneId = sceneId
	if err != nil {
		return nil, err
	}
	d.SessionId = r.AssignInfo.SessionId
	d.SessionType = r.AssignType
	d.HeatTime = time.Now().Unix()
	d.initChan()
	return r, err
}

/**
 * @Description: 获取robot类型的会话 TODO：暂未实现
 * @receiver d
 * @return *option.RobotSessionResp
 * @return error
 */
func (d *UDesk) GetRobotSession() (*option.RobotSessionResp, error) {
	return d.CreateRobotSession()
}

/**
 * @Description: 创建robot类型的会话，用来实现问题关键字交互 TODO：暂未实现
 * @receiver d
 * @return *option.RobotSessionResp
 * @return error
 */
func (d *UDesk) CreateRobotSession() (*option.RobotSessionResp, error) {
	url := d.GetUDeskUrl("session")
	r := new(option.RobotSessionResp)
	body := option.NewRobotSessionOption(d.CustomerToken)
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	if err != nil {
		return nil, err
	}
	d.SessionType = r.AssignType
	d.HeatTime = time.Now().Unix()
	d.initChan()
	return r, err
}

/**
 * @Description: 由机器人会话切换到客服会话时使用，会向udesk发送转客服记录
 * @receiver d
 * @return *option.AgentSessionResp
 * @return error
 */
func (d *UDesk) TurnAgent() (*option.AgentSessionResp, error) {
	_, err := d.TransferTrigger()
	if err != nil {
		return nil, err
	}
	return d.CreateAgentSession()
}

/**
 * @Description: 关闭与udesk的会话
 * @receiver d
 */
func (d UDesk) CloseSession() (*option.SessionCloseResp, error) {
	url := fmt.Sprintf("%v/%v", d.GetUDeskUrl("session"), d.SessionId)
	r := new(option.SessionCloseResp)
	body := option.NewAgentSessionOption(d.CustomerToken)
	_, err := d.Request().SetBody(body).SetResult(r).Delete(url)
	return r, err
}

/**
 * @Description: 获取客服排队信息
 * @receiver d
 * @return *option.GetQueueStateResp
 * @return error
 */
func (d UDesk) GetQueueState() (*option.GetQueueStateResp, error) {
	url := d.GetUDeskUrl("queue_state")
	r := new(option.GetQueueStateResp)
	_, err := d.Request().SetQueryParam("customer_token", d.CustomerToken).SetResult(r).Get(url)
	return r, err
}
