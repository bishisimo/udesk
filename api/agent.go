// @Author：bishisimo  2021/4/9 下午4:38
// @Description：客服相关api
package api

import (
	"gitee.com/bishisimo/udesk/option"
)

/**
 * @Description: 获取客服列表
 * @receiver c
 * @return *option.AgentsResp 客服列表响应
 * @return error
 */
func (c uDeskCore) GetAgents() (*option.AgentsResp, error) {
	url := c.GetUDeskUrl("agents")
	r := new(option.AgentsResp)
	_, err := c.Request().SetResult(r).Get(url)
	return r, err
}
