// @Author：bishisimo  2021/4/1 下午3:25
// @Description：
package api

import (
	"gitee.com/bishisimo/udesk/option"
)

/**
 * @Description: 获取机器人列表
 * @receiver c
 * @return *option.URobotListResp
 * @return error
 */
func (c uDeskCore) GetURobots() (*option.URobotListResp, error) {
	url := c.GetUDeskUrl("urobots")
	r := new(option.URobotListResp)
	_, err := c.Request().SetResult(r).Get(url)
	return r, err
}

/**
 * @Description: 机器人问题评价
 * @receiver d
 * @param messsageLogId
 * @param option_id
 * @return *option.URobotAnswerAppraiseResp
 * @return error
 */
func (d UDesk) URobotAnswerAppraise(messsageLogId int, option_id int) (*option.URobotAnswerAppraiseResp, error) {
	url := d.GetUDeskUrl("urobot_answer_survey")
	r := new(option.URobotAnswerAppraiseResp)
	body := &option.URobotAnswerAppraiseOption{
		CustomerToken:  d.CustomerToken,
		MessageLogId:   messsageLogId,
		ImSubSessionId: d.SessionId,
		OptionId:       option_id,
		RobotId:        d.RobotId,
		SceneId:        d.SceneId,
	}
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	return r, err
}

/**
 * @Description: 机器人会话评价
 * @receiver d
 * @param option_id
 * @return *option.URobotSessionAppraiseResp
 * @return error
 */
func (d UDesk) URobotSessionAppraise(option_id int) (*option.URobotSessionAppraiseResp, error) {
	url := d.GetUDeskUrl("urobot_session_appraise")
	r := new(option.URobotSessionAppraiseResp)
	body := &option.URobotSessionAppraiseOption{
		CustomerToken:   d.CustomerToken,
		ImSubSession_Id: d.SessionId,
		OptionId:        option_id,
		Remark:          "",
		RobotId:         d.RobotId,
		SceneId:         d.SceneId,
	}
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	return r, err
}

/**
 * @Description: 转客服的udesk触发器，udesk通过此记录
 * @receiver d
 * @return *option.TransferAgentTriggerResp
 * @return error
 */
func (d UDesk) TransferTrigger() (*option.TransferAgentTriggerResp, error) {
	url := d.GetUDeskUrl("transfer")
	r := new(option.TransferAgentTriggerResp)
	body := &option.TransferAgentTriggerOption{
		ImSubSessionId: d.SessionId,
		RobotId:        d.RobotId,
		SceneId:        d.SceneId,
	}
	_, err := d.Request().SetBody(body).SetResult(r).Post(url)
	return r, err
}
