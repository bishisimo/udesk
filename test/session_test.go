package test

import (
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"testing"
)

/**
 * @Description: 测试获取token
 * @param t
 */
func TestGetToken(t *testing.T) {
	if ctl.UDeskToken == "" {
		t.Fail()
		return
	}
	utils.Println(ctl.UDeskToken)
}

/**
 * @Description: 测试获取机器人列表
 * @param t
 */
func TestGetRobots(t *testing.T) {
	uDesk := ctl.NewUDesk("test")
	r, err := uDesk.GetURobots()
	utils.Println(r)
	utils.SaveJson("robots", r)
	if err != nil || r.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}

/**
 * @Description: 测试客服会话创建
 * @param t
 */
func TestCreateAgentSession(t *testing.T) {
	uDesk := ctl.NewUDesk("test")
	r, err := uDesk.CreateAgentSession()
	utils.Println(r)
	utils.SaveJson("agent_session", r)
	if err != nil || r.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
	state, err := uDesk.GetQueueState()
	utils.Println(state)
	if err != nil {
		t.Fail()
		return
	}
}

/**
 * @Description: 测试urobot会话创建
 * @param t
 */
func TestCreateURobotSession(t *testing.T) {
	err := ctl.RefreshToken()
	if err != nil {
		t.Fail()
	}
	uDesk := ctl.NewUDesk("test")
	uRobotRespond, err := uDesk.GetURobots()
	utils.LogErr(err)
	index := 2
	uRobot := uRobotRespond.URobotList[index]
	scene := uRobot.SceneList[0]
	robotId := uRobot.Id
	sceneId := scene.Id
	r, err := uDesk.CreateURobotSession(robotId, sceneId)
	utils.Println(r)
	utils.SaveJson("urobot_session", r)
	if err != nil || r.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}

/**
 * @Description: 测试机器人会话创建
 * @param t
 */
func TestCreateRobotSession(t *testing.T) {
	err := ctl.RefreshToken()
	if err != nil {
		t.Fail()
	}
	uDesk := ctl.NewUDesk("test")
	r, err := uDesk.CreateRobotSession()
	utils.Println(r)
	if err != nil {
		utils.LogErr(err)
		t.Fail()
	}
}
