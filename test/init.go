// @Author：bishisimo  2021/7/17 16:51
// @Description
package test

import (
	"gitee.com/bishisimo/udesk/api"
	_ "gitee.com/bishisimo/udesk/config"
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"github.com/spf13/viper"
)

var ctl *api.UDeskCtl

func init() {
	viper.SetConfigFile("../config/dev.toml")
	err := viper.ReadInConfig()
	if err != nil {
		panic("config file can not find")
		return
	}
	ctl = newCtl()
	utils.LogErr(ctl.RefreshToken())
}

func newCtl() *api.UDeskCtl {
	o := &option.UDeskOption{
		Email:    viper.GetString("udesk.email"),
		Password: viper.GetString("udesk.password"),
		Domain:   viper.GetString("udesk.domain"),
		Api:      viper.GetString("udesk.api"),
	}
	return api.NewUDeskCtlByOption(o)
}
