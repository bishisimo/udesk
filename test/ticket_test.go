// @Author：bishisimo  2021/4/7 下午8:01
// @Description：
package test

import (
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"testing"
)

/**
 * @Description: 测试工单创建
 * @param t
 */
func TestTicketCreate(t *testing.T) {
	u := ctl.NewUDesk("test")
	ticketOption := &option.TicketCreateOption{
		TicketInfo: &option.TicketInfo{
			Subject:        "test",
			Content:        "<b>咨询类别: 技术支持-操作系统-桌面</b><hr><b>客户邮箱 : tiamshilong@uniontech.com</b><hr><b>内容：</b>uos<hr><b>System Versions</b>:<br/><b>OS name: </b>统信 </b>专业版<br/><b>Architecture: </b>4.19.0-desktop-amd64<br/><b>Major version: </b>V20<br/><b>Minor version: </b>1031<br/><b>Developer mode: </b>Enabled<br/><hr><b>Authorization I</b>:<br/><b>Product ID: </b>AAASAA<br/><b>Version: </b> - - <br/><b>Serial number: </b>QWVUX8UCDFWS727CAQAGFA99N<br/><b>Give authorization to: </b>统信桌面操作系统<br/><b>Authorizatus: </b>Authorized<br/><b>Activation status: </b>Activated<br/><b>Expiration date: </b>2021-05-13<br/><b>Activation method: </b>KMS Tools<br/><b>Authorization server: </b> - - <br/><hr>",
			Type:           "token",
			TypeContent:    "bishisimo_559399",
			Priority:       "",
			AssigneeEmail:  "",
			AgentGroupName: "",
			TemplateId:     0,
			FollowerIds:    []int{},
			Tags:           "",
			Status:         "",
			TicketField:    map[string]string{},
			CreatorEmail:   "tianshilong@uniontech.com",
		},
	}
	ticket, err := u.CreateTicket(ticketOption)
	utils.Println(ticket)
	if err != nil {
		t.Fail()
		return
	}
	attach, err := u.TicketAttach(ticket.TicketId, "url.yaml", "config/url.yaml")
	utils.Println(attach)
	if err != nil {
		t.Fail()
		return
	}
}
