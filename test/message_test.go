package test

import (
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"testing"
)

/**
 * @Description: 测试urobot通信
 * @param t
 */
func TestURobotIM(t *testing.T) {
	uDesk := ctl.NewUDesk("uos-bishisimo-f23765d4")
	uRobotRespond, err := uDesk.GetURobots()
	if err != nil || uRobotRespond.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
	uRobot := uRobotRespond.URobotList[2]
	scene := uRobot.SceneList[0]
	robotId := uRobot.Id
	sceneId := scene.Id
	uRobotSessionResp, err := uDesk.CreateURobotSession(robotId, sceneId)
	utils.Println("uRobotSessionResp", uRobotSessionResp)
	if err != nil || uRobotSessionResp.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
	messageResp, err := uDesk.SendMessageURobot("将速诺片剂掰开使用,对效果有影响吗?")
	utils.Println("messageResp", messageResp)
	if err != nil || messageResp.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
	////会话评价
	//sessionSurveyResp, err := uDesk.URobotSessionAppraise(option.URobotSessionSatisfied)
	//utils.Println("sessionSurveyResp", sessionSurveyResp)
	//if err != nil || sessionSurveyResp.Code != option.SuccessCode {
	//	utils.LogErr(err)
	//	t.Fail()
	//}
	//问题评价，需要回答logId暂时无法测试
	//questionSurveyResp,err:=uDesk.URobotAnswerAppraise(3192524,option.URobotQuestionSatisfied)
	//utils.Println("questionSurveyResp",questionSurveyResp)
	//if err != nil || questionSurveyResp.Code != option.SuccessCode {
	//	utils.LogErr(err)
	//	t.Fail()
	//}
	//转客服
	transAgentResp, err := uDesk.TurnAgent()
	utils.Println("transAgentResp", transAgentResp)
	if err != nil || transAgentResp.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}

/**
 * @Description: 测试客服通信
 * @param t
 */
func TestAgentIM(t *testing.T) {
	uDesk := ctl.NewUDesk("test")
	sessionResp, err := uDesk.GetAgentSession()
	if err != nil || sessionResp.Code != option.SuccessCode && sessionResp.Code != option.VainCode {
		utils.LogErr(err)
		t.Fail()
	}
	data := &option.MessageData{
		Font:     "",
		Content:  "测试问题",
		Filename: "",
		Filesize: "",
		Duration: 0,
	}
	messageResp, err := uDesk.SendMessageAgent(uDesk.SessionId, "1", option.MessageTypeEnum.Message, data)
	if err != nil || messageResp.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}
