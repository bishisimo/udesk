// @Author：bishisimo  2021/4/9 下午4:44
// @Description：
package test

import (
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"testing"
)

/**
 * @Description: 测试获取客服列表
 * @param t
 */
func TestAgents(t *testing.T) {
	uDesk := ctl.NewUDesk("test")
	listResp, err := uDesk.GetAgents()
	utils.Println(listResp)
	utils.SaveJson("agents", listResp)
	if err != nil || listResp.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}
