// @Author：bishisimo   13:35
// @Description
package test

import (
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"testing"
)

/**
 * @Description: 测试创建创建客户
 * @param t
 */
func TestCreateCustomer(t *testing.T) {
	u := ctl.NewUDesk("bishisimo_559399")
	customer, err := u.CreateCustomer("", "bishisimo")
	utils.Println(customer)
	utils.SaveJson("customer", customer)
	if err != nil || customer.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}

/**
 * @Description: 测试获取客户信息
 * @param t
 */
func TestGetCustomerInfo(t *testing.T) {
	token := "bishisimo_559399"
	info, err := ctl.GetCustomerInfo(option.CustomerTypeEnum.Token, token)
	utils.Println(info)
	if err != nil || info.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}

/**
 * @Description: 测试获取或者创建客户
 * @param t
 */
func TestGetOrCreateCustomer(t *testing.T) {
	u := ctl.NewUDesk("bishisimo_559399")
	customer, err := u.GetOrCreateCustomer("", "bishisimo")
	utils.Println(customer)
	utils.SaveJson("customer", customer)
	if err != nil || customer.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}

/**
 * @Description: 测试更新客户
 * @param t
 */
func TestUpdateCustomer(t *testing.T) {
	u := ctl.NewUDesk("bishisimo_559399")
	customer, err := u.UpdateCustomer("tianshilong@uniontech.com")
	utils.Println(customer)
	if err != nil {
		t.Fail()
	}
}

/**
 * @Description: 测试删除客户
 * @param t
 */
func TestDeleteCustomer(t *testing.T) {
	u := ctl.NewUDesk("bishisimo_559399")
	customer, err := u.DeleteCustomer()
	utils.Println(customer)
	if err != nil {
		t.Fail()
		return
	}
}
