// @Author：bishisimo  2021/4/7 下午6:48
// @Description：
package test

import (
	"gitee.com/bishisimo/udesk/option"
	"gitee.com/bishisimo/udesk/utils"
	"testing"
)

/**
 * @Description: 测试urobot评价
 * @param t
 */
func TestURobotSurvey(t *testing.T) {
	uDesk := ctl.NewUDesk("test")
	uRobotRespond, err := uDesk.GetURobots()
	if err != nil || uRobotRespond.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
	uRobot := uRobotRespond.URobotList[2]
	scene := uRobot.SceneList[0]
	uDesk.RobotId = uRobot.Id
	uDesk.SceneId = scene.Id
	uDesk.SessionId = 321713186
	//问题评价
	questionSurveyResp, err := uDesk.URobotAnswerAppraise(946800215, option.URobotQuestionSatisfied)
	utils.Println("questionSurveyResp", questionSurveyResp)
	if err != nil || questionSurveyResp.Code != option.SuccessCode {
		utils.LogErr(err)
		t.Fail()
	}
}
