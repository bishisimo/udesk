package utils

import (
	"bytes"
	"encoding/binary"
	"github.com/google/uuid"
	"strings"
)

func GenerateUUID() string {
	return uuid.New().String()
}

func GenerateUUIDWithOutLine() string {
	return strings.ReplaceAll(GenerateUUID(), "-", "")
}

// uuid 转为 19 位数字
func GenerateUUIDNumber() uint64 {
	var res uint64
	u := uuid.New()
	bf, _ := u.MarshalBinary()
	buf := bytes.NewReader(bf)
	_ = binary.Read(buf, binary.LittleEndian, &res)
	return res
}
