package utils

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

//打印错误，包含调用者函数名
func LogErr(err error) {
	if err != nil {
		zerolog.CallerSkipFrameCount = 3
		log.Error().Stack().Err(err).Send()
		zerolog.CallerSkipFrameCount = 2
	}
}

//打印空数据或者错误信息
func LogNilOrErr(data interface{}, err error) {
	if err != nil {
		zerolog.CallerSkipFrameCount = 3
		log.Error().Stack().Err(err).Send()
		zerolog.CallerSkipFrameCount = 2
	} else if data == nil {
		zerolog.CallerSkipFrameCount = 3
		log.Error().Stack().Msg("data is nil")
		zerolog.CallerSkipFrameCount = 2
	}
}
