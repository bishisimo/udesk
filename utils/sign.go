package utils

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/spf13/viper"
	"time"
)

func Sha256(s string) string {
	bytes2 := sha256.Sum256([]byte(s))
	return hex.EncodeToString(bytes2[:])
}

func UDeskSignBase(email string, token string, ts int64, nonce string, version string) string {
	s := fmt.Sprintf("%v&%v&%v&%v&%v", email, token, ts, nonce, version)
	return Sha256(s)
}

/**
 * @Description: udeks签名
 * @param token udesk token
 * @param ts 当前时间戳
 * @param nonce
 * @return string
 */
func UDeskSign(token string, ts int64, nonce string) string {
	email := viper.GetString("udesk.email")
	if ts == 0 {
		ts = time.Now().Unix()
	}
	if nonce == "" {
		nonce = GenerateUUID()
	}
	return UDeskSignBase(email, token, ts, nonce, "v2")
}
