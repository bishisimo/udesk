package utils

import (
	"encoding/json"
	"os"
	"path"
)

/**
 * @Description: 保存udesk返回的数据，用来调试
 * @param name 保存的文件名
 * @param data 保存的数据
 */
func SaveJson(name string, data interface{}) {
	s, err := json.Marshal(data)
	LogErr(err)
	f, err := os.OpenFile(path.Join("../data", name+".json"), os.O_WRONLY|os.O_CREATE, os.ModePerm)
	if err != nil {
		LogErr(err)
		return
	}
	_, err = f.Write(s)
	LogErr(err)
}
