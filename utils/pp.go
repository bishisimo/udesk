package utils

import (
	"github.com/k0kubun/pp"
	"runtime"
)

//获取执行的函数名，即调用GetFuncName的函数名
func GetFuncName() string {
	pc, _, _, _ := runtime.Caller(1)
	return runtime.FuncForPC(pc).Name()
}

//获取调用者函数名，即调用使用了GetCallerName函数的父函数名
func GetCallerName() string {
	pc, _, _, _ := runtime.Caller(2)
	return runtime.FuncForPC(pc).Name()
}

func Println(a ...interface{}) {
	_, _ = pp.Println(GetCallerName(), a)
}

func Printf(format string, a ...interface{}) {
	_, _ = pp.Printf(GetCallerName(), format, a)
}
