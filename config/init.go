// @Author：bishisimo  2021/4/9 下午4:38
// @Description：配置文件管理，可传入多个文件夹路径，将url.yaml拷贝至config.yaml
package config

import (
	"gitee.com/bishisimo/udesk/option"
	"github.com/spf13/viper"
)

func init() {
	urlList := map[string]string{
		"urobots":                 "/im/urobots",
		"token":                   "/log_in",
		"session":                 "/im/sessions",
		"customer_info":           "/customers/get_customer",
		"customer_create":         "/customers",
		"customer_update":         "/customers/update_customer",
		"customer_delete":         "/customers/destroy_customer",
		"urobot_session_appraise": "/im/sessions/robot_survey",
		"urobot_answer_appraise":  "/im/messages/answer_survey",
		"message":                 "/im/messages",
		"ticket":                  "/tickets",
		"ticket_detail":           "/tickets/detail",
		"ticket_delete":           "/tickets/destroy_ticket",
		"ticket_attach":           "/tickets/upload_file",
		"flow":                    "/im/messages/flow",
		"hit":                     "/im/messages/hit",
		"notice":                  "/set_notice_url",
		"transfer":                "/im/sessions/transfer_survey",
		"agents":                  "/agents",
		"queue_state":             "/im/queue_status",
	}
	for k, v := range urlList {
		viper.SetDefault(option.UDeskUrlPrefix+k, v)
	}
}
