module gitee.com/bishisimo/udesk

go 1.16

require (
	github.com/go-resty/resty/v2 v2.6.0
	github.com/google/uuid v1.2.0
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.23.0
	github.com/spf13/viper v1.7.1
	golang.org/x/net v0.0.0-20210508051633-16afe75a6701 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
