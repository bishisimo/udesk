package option

/**
 * @Description: token刷新请求选项
 */
type TokenOption struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

/**
 * @Description: 通过参数构造token刷新结构体
 * @param email udeks账户邮箱
 * @param password udesk账户密码
 * @return *TokenOption token刷新请求选项
 */
func NewTokenOption(email string, password string) *TokenOption {
	return &TokenOption{Email: email, Password: password}
}

/**
 * @Description: 刷新token响应
 */
type TokenResp struct {
	Code             int    `json:"code"`
	OpenApiAuthToken string `json:"open_api_auth_token"` //udesk分配的新token
	Message          string `json:"message"`
}
