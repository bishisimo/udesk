package option

/**
 * @Description: 通用带code返回
 */
type RespWithCode struct {
	Code int `json:"code"`
}

/**
 * @Description: 通用带code和message的返回
 */
type RespWithCodeAndMessage struct {
	Code    int    `json:"code"`    //　　　　　	执行结果码,1000表示成功
	Message string `json:"message"` //　　　	结果说明
}

/**
 * @Description: 分页信息
 */
type PageInfo struct {
	CurrentPage int `json:"current_page"` //	当前页号
	TotalPages  int `json:"total_pages"`  //	总页数
	TotalCount  int `json:"total_count"`  //数据总量
}
