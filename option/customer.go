package option

/**
 * @Description: 创建客户的选项
 */
type CustomerCreateOption struct {
	CustomerInfo `json:"customer"`
	OtherEmail   [][]string `json:"other_email"`
	Tags         string     `json:"tags"`
}

/**
 * @Description: 创建客户的客户信息
 */
type CustomerInfo struct {
	Id            int    `json:"id"`
	Email         string `json:"email"` //主邮箱地址
	OpenApiToken  string `json:"open_api_token"`
	NickName      string `json:"nick_name"`
	CreateAt      string `json:"create_at"`
	UpdatedAt     string `json:"updated_at"`
	Lang          string `json:"lang"`
	SourceChannel string `json:"source_channel"`
	Platform      string `json:"platform"`
	//OrganizationId int         `json:"organization_id"`
	//Description    string      `json:"description"`
	//OwnerId        int         `json:"owner_id"`
	//OwnerGroupId   int         `json:"owner_group_id"`
	//Level          string      `json:"level"`
	//IsBlocked      bool        `json:"is_blocked"`
	//Cellphones     []string    `json:"cellphones"`
	//WeChats        []string    `json:"weixins"`
	//WeChatsApp     []string    `json:"weixin_minis"`
	//WeChatCom      []string    `json:"weixin_works"`
	//CustomFields   interface{} `json:"custom_fields"`
	//WebToken       string      `json:"web_token"`
}

/**
 * @Description: 创建客户的返回
 */
type CustomerCreateResp struct {
	Code         int               `json:"code"` //	执行结果码，1000代表成功
	CustomerInfo `json:"customer"` //	客户信息，参见客户数据
}

////////////////////////////////

/**
 * @Description: 客户信息类型的枚举
 */
var CustomerTypeEnum = customerTypeEnum{
	Id:                   "id",
	Email:                "email",
	Cellphone:            "cellphone",
	Token:                "token",
	WechatOpenId:         "weixin_open_id",
	WechatMiniOpenid:     "weixin_mini_openid",
	WechatWorkIdentifier: "weixin_work_identifier",
	WeiboId:              "weibo_id",
	SdkToken:             "sdk_token",
	WebToken:             "web_token",
}

type customerTypeEnum struct {
	Id                   string // 客户id
	Email                string // 客户邮箱
	Cellphone            string //	客户电话
	Token                string // 客户外部唯一标识
	WechatOpenId         string // 客户微信openid
	WechatMiniOpenid     string // 客户微信小程序openid
	WechatWorkIdentifier string // 客户企业微信的唯一标识，例：cropid:wxc727955fe6025ed4,agentid:1009117,userid:LS004308
	WeiboId              string //	客户微博openid
	SdkToken             string //	客户sdk标识
	WebToken             string //	客户web标识
}

/**
 * @Description: 客户查询返回结果
 */
type CustomerQueryResp = CustomerCreateResp

/////////////////////////////////
/**
 * @Description: 客户更新选项
 */
type CustomerUpdateOption = CustomerCreateOption

/**
 * @Description: 客户更新返回结果
 */
type CustomerUpdateResp = CustomerCreateResp

///////////////////////////
/**
 * @Description: 客户删除返回结果
 */
type CustomerDeleteResp struct {
	Code       int `json:"code"`
	CustomerId int `json:"customer_id"`
}
