package option

import (
	"encoding/json"
	"errors"
)

/**
 * @Description: 会话创建选项
 */
type SessionOption struct {
	CustomerToken string `json:"customer_token" option:"required"` //应用端客户唯一标识
	AssignType    string `json:"assign_type"`                      //期望的分配类型, 'robot', 'urobot', 'agent', 默认为 'robot'
	AgentId       int    `json:"agent_id"`                         //指定分配的客服id，如果指定忽略 assign_type 和 group_id
	GroupId       int    `json:"group_id"`                         //指定分配的客服组id，如果指定忽略 assign_type
	Channel       string `json:"channel"`                          //自定义渠道
	RobotRoleId   string `json:"robot_role_id"`                    //客户角色
	RobotId       int    `json:"robot_id"`                         //udesk机器人id
	SceneId       int    `json:"scene_id"`                         //udesk机器人对应场景id
}

/**
 * @Description: 创建urobot会话选项
 * @param CustomerToken 用户标识符
 * @param RobotRoleId 机器人角色id
 * @param RobotId 机器人id
 * @param SceneId 场景id
 * @return *SessionOption
 */
func NewURobotSessionOption(CustomerToken string, RobotRoleId string, RobotId int, SceneId int) *SessionOption {
	return &SessionOption{
		CustomerToken: CustomerToken,
		RobotRoleId:   RobotRoleId,
		RobotId:       RobotId,
		SceneId:       SceneId,
		AssignType:    "urobot",
	}
}

/**
 * @Description: 创建客服会话选项
 * @param CustomerToken 用户标识符
 * @return *SessionOption
 */
func NewAgentSessionOption(CustomerToken string) *SessionOption {
	return &SessionOption{
		CustomerToken: CustomerToken,
		AssignType:    "agent",
	}
}

/**
 * @Description: 使用参数创建机器人会话选项
 * @param CustomerToken 用户标识符
 * @return *SessionOption
 */
func NewRobotSessionOption(CustomerToken string) *SessionOption {
	return &SessionOption{
		CustomerToken: CustomerToken,
		AssignType:    "robot",
		RobotRoleId:   "桌面",
	}
}

/**
 * @Description: 会话关闭选项
 */
type SessionCloseOption struct {
	SessionId int `json:"session_id"` //创建会话分配的id
}

/**********************************************************************************************************************/
/**
 * @Description: 会话创建的响应
 */
type SessionResp struct {
	Code       int    `json:"code"`        //执行结果码，1000代表成功
	Message    string `json:"message"`     //分配成功时为欢迎语，失败时为错误提示
	AssignType string `json:"assign_type"` //分配类型, 'robot', 'agent', 'urobot'
	AssignInfo string `json:"assign_info"` //根据类型进一步反序列化
}

/**
 * @Description: 创建机器人会话的响应
 */
type RobotSessionResp struct {
	SessionResp
	AssignInfo RobotAssignInfo `json:"assign_info"`
}

/**
 * @Description: urobot机器人会话创建的响应
 */
type URobotSessionResp struct {
	SessionResp
	AssignInfo URobotAssignInfo `json:"assign_info"`
}

/**
 * @Description: 客服会话创建的响应
 */
type AgentSessionResp struct {
	SessionResp
	AssignInfo AgentAssignInfo `json:"assign_info"`
}

/**
 * @Description: 反序列化分配信息
 * @receiver s 会话创建响应
 * @return r 不同类型的分配信息体
 * @return err 意外分配类型
 */
func (s SessionResp) UnmarshalAssignInfo() (r interface{}, err error) {
	switch s.AssignType {
	case "robot":
		r = new(RobotAssignInfo)
	case "urobot":
		r = new(URobotAssignInfo)
	case "agent":
		r = new(AgentAssignInfo)
	default:
		return nil, errors.New("AssignType is Error")
	}
	err = json.Unmarshal([]byte(s.AssignInfo), r)
	return
}

/**
 * @Description: 机器人分配信息体
 */
type RobotAssignInfo struct {
	RobotName      string `json:"robot_name"`      //机器人姓名
	RobotAvatar    string `json:"robot_avatar"`    //机器人头像
	WelcomeMessage string `json:"welcome_message"` //欢迎语
	UnknownMessage string `json:"unknown_message"` //未知回答语
}

/**
 * @Description: urobot机器人分配信息体
 */
type URobotAssignInfo struct {
	SessionId         int      `json:"sessionId"`   //会话id
	LogId             int      `json:"logId"`       //该条log的id
	LeadingWord       string   `json:"leadingWord"` //引导语
	HelloWord         string   `json:"helloWord"`   //欢迎语
	RobotName         string   `json:"robotName"`   //机器人名称
	LogoUrl           string   `json:"logoUrl"`     //机器人头像
	TopAsk            []Ask    `json:"topAsk"`      //常见问题列表
	SwitchStaffAnswer string   `json:"switchStaffAnswer"`
	SatisfactionFlag  int      `json:"satisfactionFlag"`
	EnableRefresh     bool     `json:"enableRefresh"`
	RefreshWindowSize int      `json:"refresh_window_size"`
	PersonifyRobot    bool     `json:"personify_robot"`
	ShortcutEntryList []string `json:"shortcutEntryList"`
}

/**
 * @Description: 询问问题信息体
 */
type Ask struct {
	QuestionType   string         `json:"questionType"`   //问题类型
	QuestionTypeId int            `json:"questionTypeId"` //问题类型id
	OptionsList    []QuestionItem `json:"optionsList"`    //类型候选问题
}

/**
 * @Description: 问题内容信息体
 */
type QuestionItem struct {
	Question   string `json:"question"`   //问题
	QuestionId int    `json:"questionId"` //问题id
}

/**
 * @Description: 客服分配信息体
 */
type AgentAssignInfo struct {
	ImSubSessionId int                    `json:"im_sub_session_id"` //会话ID
	Count          int                    `json:"count"`             //排队位置
	Queue          string                 `json:"queue"`             //排队队列
	AgentId        int                    `json:"agent_id"`          //分配的客服id
	AgentName      string                 `json:"agent_name"`        //分配的客服名称
	AgentAvatar    string                 `json:"agent_avatar"`      //客服头像地址
	SurveyOptions  map[string]interface{} `json:"survey_options"`    //满意度评价设置
}

/**
 * @Description: 关闭会话响应
 */
type SessionCloseResp struct {
	Code int `json:"code"`
}

//////////////////////////////////////
/**
 * @Description: 获取队列请求响应
 */
type GetQueueStateResp struct {
	Code   int    `json:"code"`   //	执行结果码，1000代表成功
	Status string `json:"status"` //	排队状态, '排队中', '未排队', '会话中', '分配中'
	Count  int    `json:"count"`  //	排队位置
}
