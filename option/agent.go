// @Author：bishisimo  2021/4/9 下午4:26
// @Description：
package option

/**
 * @Description: 查询客服列表的请求控制选项
 */
type AgentsOption struct {
	Page         int  `json:"page"`          //	页码，从1开始，默认为1
	PerPage      int  `json:"per_page"`      //	每页数量，默认20，最大100
	WithDisabled bool `json:"with_disabled"` //是否包含已禁用客服，默认为false（不包含已禁用客服）
}

/**
 * @Description: 查询客服的响应
 */
type AgentsResp struct {
	Code   int         `json:"code"`   //	执行结果码，1000代表成功
	Meta   PageInfo    `json:"meta"`   //	分页信息，详见通用数据
	Agents []AgentInfo `json:"agents"` //	客服列表，每个客服的说明参见客服数据
}

/**
 * @Description: 单个客服的信息
 */
type AgentInfo struct {
	Id                        int          `json:"id"`                           //	唯一标识
	Email                     string       `json:"email"`                        //	邮箱地址
	NickName                  string       `json:"nick_name"`                    //	姓名
	Profile                   string       `json:"profile"`                      //	员工类型
	Alias                     string       `json:"aliase"`                       //	外显昵称
	Cellphone                 string       `json:"cellphone"`                    //手机号码
	RoleName                  string       `json:"role_name"`                    //	角色
	Duty                      string       `json:"duty"`                         //	员工职务
	ImAbilityValue            int          `json:"im_ability_value"`             //	对话技能值
	UserGroupIds              []int        `json:"user_group_ids"`               //	所属客服组id列表
	Password                  string       `json:"password"`                     //	呼叫中心SIP账号信息
	Number                    string       `json:"number"`                       //	呼叫中心SIP账号信息
	Avatar                    string       `json:"avatar"`                       //	头像
	WorkId                    string       `json:"work_id"`                      //	工号
	Departments               []Department `json:"departments"`                  //	所属部门，包括id（部门id），name（部门名称），详见示例
	AgentCalloutDisplayNumber string       `json:"agent_callout_display_number"` //	外呼显号
	DisableStatus             string       `json:"disable_status"`               //	状态
	Availability              bool         `json:"availability"`                 //	是否接收工单自动分配
	ImWelcomes                string       `json:"im_welcomes"`                  //	欢迎语
	Lang                      string       `json:"lang"`                         //	语言偏好
}

/**
 * @Description: 客服部门信息
 */
type Department struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}
