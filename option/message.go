package option

/**
 * @Description: 发型给udeks消息的选项
 */
type MessageOption struct {
	CustomerToken   string      `json:"customer_token"`    //	是	应用端客户唯一标识
	ImSubSessionId  int         `json:"im_sub_session_id"` //	是	1. 会话ID 2. ID为0或空为机器人问题 3. ID为-1为Udesk机器人问题 4. -2为IM工作台留言 5. -3为排队中发送
	MessageId       string      `json:"message_id"`        //	是	id/公司唯一标识（Udesk机器人可不传） 仅支持字母,数字,"_","-",禁用特殊字符
	Type            string      `json:"type"`              //否	类型, 'message', 'image', 'audio', 'rich', 默认为 'message'
	Data            MessageData `json:"data"`              //	是	内容 (详情见 内容格式)
	RobotId         int         `json:"robot_id"`          //	否	Udesk机器人id(Udesk机器人必填)
	SceneId         int         `json:"scene_id"`          //	否	Udesk机器人对应场景id(Udesk机器人必填)
	URobotSessionId int         `json:"urobot_session_id"` //	否	Udesk机器人对应的会话id
	AgentId         int         `json:"agent_id"`          //	否	指定分配的客服id 配合im_sub_session_id=-2使用
	GroupId         int         `json:"group_id"`          //	否	指定分配的客服组id 配合im_sub_session_id=-2使用
}

/**
 * @Description: 消息体内容，根据不同形式有不同，具体详见 https://www.udesk.cn/doc/apiv2/im/#_40
 */
type MessageData struct {
	Font     string `json:"font"`
	Content  string `json:"content"`
	Filename string `json:"filename"` // 文件名
	Filesize string `json:"filesize"` // 文件大小
	Duration int    `json:"duration"` // 语音消息持续时长
}

/**
 * @Description: 暂未使用，详见 https://www.udesk.cn/doc/apiv2/im/#_55
 */
type FlowOption struct {
	CustomerToken  string `json:"customer_token"`    //	是	应用端客户唯一标识
	ImSubSessionId int    `json:"im_sub_session_id"` //	是	会话id
	FlowId         int    `json:"flow_id"`           //	是	选中的flow_id
	RobotId        int    `json:"robot_id"`          //	是	udesk机器人id
	SceneId        int    `json:"scene_id"`          //	是	udesk机器人对应场景id
}

/**
 * @Description: 暂未使用，详见 https://www.udesk.cn/doc/apiv2/im/#_59
 */
type HitOption struct {
	CustomerToken  string `json:"customer_token"`    //	是	应用端客户唯一标识
	ImSubSessionId int    `json:"im_sub_session_id"` //	是	会话id
	MessageId      int    `json:"message_id"`        //	是	机器人答案的消息id
	RobotId        int    `json:"robot_id"`          //	是	机器人id
	Question       int    `json:"question"`          //	否	问题内容
	QueryType      int    `json:"query_type"`        //	是	点击类型 6：常见问题点击 7：建议列表点击
}

type MessageResp = RespWithCode

///////////////////////////////
/**
 * @Description: 消息类型枚举，详见 https://www.udesk.cn/doc/apiv2/im/#_40
 */
var MessageTypeEnum = &messageTypeEnum{
	Message:       "message",
	Image:         "image",
	Audio:         "audio",
	Video:         "video",
	File:          "file",
	Rich:          "rich",
	StartSession:  "start_session",
	Transfer:      "transfer",
	InfoTransfer:  "info_transfer",
	Close:         "close",
	Survey:        "survey",
	ActiveGuest:   "active_guest",
	InfoAppoint:   "info_appoint",
	Form:          "form",
	FormReceived:  "form_received",
	Info:          "info",
	RobotTransfer: "robot_transfer",
}

type messageTypeEnum struct {
	Message string //文本消息	✓	✓
	Image   string //图片消息	✓	✓
	Audio   string //语言消息	✓	×	支持amr格式
	Video   string //视频消息	✓	×	支持mp4格式
	File    string //文件消息	✓	×
	Rich    string //富文本消息	✓	✓	支持常见的html标签
	//Struct	string//结构话消息	×	×	暂不支持
	StartSession  string //对话开始	×	✓	对话开始时推给客户
	Transfer      string //转接事件	×	✓	客户被转接时推给客户,客户需要修改im_sub_session_id 为新的
	InfoTransfer  string //转接事件显示内容	×	×	客户被转接时显示给客服的提示内容
	Close         string //会话关闭事件	✓	✓	客户会话被关闭时推给客户
	Survey        string //满意度评价相关事件	×	×	只会在客服IM工作台显示
	ActiveGuest   string //客服主动会话事件	×	×	仅支持web访客
	InfoAppoint   string //客服分配客户事件	×	×	暂不支持
	Form          string //发送表单消息事件	×	×	暂不支持
	FormReceived  string //接受表单消息事件	×	×	暂不支持
	Info          string //询前表单 is_receive: false	×	×	仅用于客服显示
	RobotTransfer string //机器人转接对话	×	×	仅用于客服显示
}

/**
 * @Description: 回话类型枚举
 */
var SessionTypeEnum = &sessionTypeEnum{
	RobotType:  0,
	URobotType: -1,
	ImType:     -2,
	QueueType:  -3,
}

type sessionTypeEnum struct {
	RobotType  int //ID为0或空为机器人问题
	URobotType int //ID为-1为Udesk机器人问题
	ImType     int //-2为IM工作台留言
	QueueType  int //-3为排队中发送消息
}

/////////////////////////////////////////
/**
 * @Description: 通知消息通用部分
 */
type NoticeMessageCommon struct {
	CustomerToken string `json:"customer_token"`
	AssignType    string `json:"assign_type"`
}

/**
 * @Description: 客服通知消息响应
 */
type AgentNoticeMessageResp struct {
	NoticeMessageCommon
	Messages []AgentNoticeMessage `json:"messages"`
}

/**
 * @Description 客服通知消息体
 */
type AgentNoticeMessage struct {
	AgentName        string      `json:"agent_name"`
	AgentAvatar      string      `json:"agent_avatar"`
	ImSubSessionId   int         `json:"im_sub_session_id"`
	MessageCreatedAt string      `json:"message_created_at"`
	Data             MessageData `json:"data"`
	Type             string      `json:"type"`
	MessageId        string      `json:"message_id"`
	AgentId          int         `json:"agent_id"`
}

/**
 * @Description: uroboot响应
 */
type URobotNoticeMessageResp struct {
	NoticeMessageCommon
	Messages URobotMessage `json:"messages"`
}

/**
 * @Description: uroboot消息体
 */
type URobotMessage struct {
	MsgType             string        `json:"msgType"`
	AnsContent          string        `json:"ansContent"`
	SuggestQuestionList []interface{} `json:"suggestQuestionList"`
	HitQuestion         string        `json:"hitQuestion"`
	AnsType             int           `json:"ans_type"`
	SessionId           int           `json:"session_id"`
	LogId               int64         `json:"log_id"`
	Aid                 int           `json:"aid"`
}
