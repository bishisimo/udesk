// @Author：bishisimo  2021/4/7 下午7:24
// @Description：
package option

//////////////////////////////////////
/**
 * @Description: 工单创建选项
 */
type TicketCreateOption struct {
	*TicketInfo `json:"ticket"`
}

/**
 * @Description: 工单信息体
 */
type TicketInfo struct {
	Subject        string            `json:"subject"`          // 是	标题	最大长度255个字符
	Content        string            `json:"content"`          // 是	内容
	Type           string            `json:"type"`             //	否	查找客户时使用的类型
	TypeContent    string            `json:"type_content"`     //	否	与类型type对应的值	最大长度255个字符
	Priority       string            `json:"priority"`         //	否	优先级中文名称， 默认为标准
	AssigneeEmail  string            `json:"assignee_email"`   //	否	受理客服邮箱	最大长度255个字符
	AgentGroupName string            `json:"agent_group_name"` //	否	受理客服组名称	最大长度255个字符
	TemplateId     int               `json:"template_id"`      //	否	工单模板id,无传入值或传入错误值则使用默认模板
	FollowerIds    []int             `json:"follower_ids"`     //	否	工单关注人,如[1,2,3],数组内是客服id
	Tags           string            `json:"tags"`             //	否	工单标签,如"标签1,标签2",字符串内是标签名字,用逗号隔开
	Status         string            `json:"status"`           //	否	状态中文名称，默认为开启
	TicketField    map[string]string `json:"ticket_field"`     //	否	自定义字段，详见下文
	CreatorEmail   string            `json:"creator_email"`    //	否	工单创建人邮箱	最大长度255个字符
}

/**
 * @Description: 工单创建响应
 */
type TicketCreateResp struct {
	Code     int    `json:"code"`
	Message  string `json:"message"`
	TicketId int    `json:"ticket_id"` //创建的工单id
}

//////////////////////////////////////
/**
 * @Description: 工单更新选项
 */
type TicketUpdateOption struct {
	*TicketUpdate `json:"ticket"`
}

/**
 * @Description: 工单更新信息体
 */
type TicketUpdate struct {
	Subject      string            `json:"subject"`        //	是	标题	最大长度255个字符
	Content      string            `json:"content"`        //	描述
	AgentId      int               `json:"agent_id"`       //否	负责客服id
	AgentGroupId int               `json:"agent_group_id"` //	否	负责客服组id
	TemplateId   int               `json:"template_id"`    //	否	模版id，默认为默认模版
	StatusId     int               `json:"status_id"`      //	否	状态id
	PriorityId   int               `json:"priority_id"`    //	否	优先级id
	FollowerIds  []int             `json:"follower_ids"`   //否	关注者id数组，每个元素都是客服id
	CustomFields map[string]string `json:"custom_fields"`  //否	自定义字段
	Tags         string            `json:"tags"`           //	否	标签，多个标签以逗号分隔
}

/**
 * @Description: 工单更新响应
 */
type TicketUpdateResp = TicketCreateResp

//////////////////////////////////////
/**
 * @Description: 工单附件响应
 */
type TicketAttachResp struct {
	Code           int `json:"code"`
	*AttachMessage `json:"message"`
}

/**
 * @Description: 工单附件信息体
 */
type AttachMessage struct {
	Msg          string `json:"msg"`
	Url          string `json:"url"`
	AttachmentId int64  `json:"attachment_id"`
}

/*********************************************************/
/**
 * @Description: 工单细节响应
 */
type TicketDetailResp struct {
	Code          int             `json:"code"`    //　　　　	执行结果码，1000代表成功
	Message       string          `json:"message"` //　　	执行结果说明
	*TicketDetail `json:"ticket"` //对象	工单信息，结构见下
}

/**
 * @Description: 工单细节信息体
 */
type TicketDetail struct {
	Dd                int               `json:"id"`                  //	否	唯一标识，系统自动生成
	FieldNum          string            `json:"field_num"`           //	否	唯一标识，系统自动生成
	Subject           string            `json:"subject"`             //	是	标题
	Content           string            `json:"content"`             //	是	内容
	ContentType       string            `json:"content_type"`        //	是	内容类型
	UserId            int               `json:"user_id"`             //	是	客户id
	UserName          string            `json:"user_name"`           //	否　	客户姓名
	UserEmail         string            `json:"user_email"`          //	否	客户邮箱
	UserCellphone     string            `json:"user_cellphone"`      //	否	客户电话
	OrganizationId    int               `json:"organization_id"`     //	否	客户所属公司id
	AssigneeId        int               `json:"assignee_id"`         //	是	受理客服id
	AssigneeName      string            `json:"assignee_name"`       //	否	受理客服姓名
	AssigneeAvatar    string            `json:"assignee_avatar"`     //	否	受理客服头像
	UserGroupId       int               `json:"user_group_id"`       //	是	受理客服组id
	UserGroupName     string            `json:"user_group_name"`     //	否	受理客服组名称
	TemplateId        int               `json:"template_id"`         //	否　 模板id
	Priority          string            `json:"priority"`            //	是	优先级中文名称
	Status            string            `json:"status"`              //	是	状态中文名称
	StatusEn          string            `json:"status_en"`           //	是	状态英文名称
	Platform          string            `json:"platform"`            //	否	渠道中文名称
	Satisfaction      string            `json:"satisfaction"`        //	否	满意度调查结果
	CustomFields      map[string]string `json:"custom_fields"`       //	是	自定义字段，详见示例
	Tags              string            `json:"tags"`                //	是	标签
	Followers         []Follower        `json:"followers"`           //	否	关注者，包括id（关注者id）、 nick_name（关注者姓名）
	CreatorId         int               `json:"creator_id"`          //	否	创建人
	Created_At        string            `json:"created_at"`          //	否	创建时间，系统自动生成
	UpdatedAt         string            `json:"updated_at"`          //	否	更新时间，系统自动生成
	SolvingAt         string            `json:"solving_at"`          //	否	开始解决时间
	ResolvedAt        string            `json:"resolved_at"`         //	否	解决时间
	ClosedAt          string            `json:"closed_at"`           //	否	关闭时间
	SolvedDeadline    string            `json:"solved_deadline"`     //	否	到期时间
	RepliedAt         string            `json:"replied_at"`          //	否	最后回复时间
	AgentRepliedAt    string            `json:"agent_replied_at"`    //	否	客服最后回复时间
	CustomerRepliedAt string            `json:"customer_replied_at"` //	否	客户最后回复时间
	RepliedBy         string            `json:"replied_by"`          // 否	最后回复人
	Attachments       []Attachment      `json:"attachments"`         //	否	工单关联的附件，包括file_name（文件名）、url（文件URL）
	ImSubSessionId    int               `json:"im_sub_session_id"`   //	否	工单的会话ID
}

/**
 * @Description: 关注者信息体
 */
type Follower struct {
	Id       int    `json:"id"`        //关注者id
	NickName string `json:"nick_name"` //关注者姓名
}

/**
 * @Description: 附件信息体
 */
type Attachment struct {
	FileName string `json:"file_name"` //文件名
	Url      string `json:"url"`       //文件URL
}

//////////////////////////////////////
/**
 * @Description: 工单删除响应
 */
type TicketDeleteResp = RespWithCodeAndMessage

//////////////////////////////////////
