package option

/////////////////////////////////////////
/**
 * @Description: urobot会话评价选项
 */
type URobotSessionAppraiseOption struct {
	CustomerToken   string `json:"customer_token"`    //是	应用端客户唯一标识
	ImSubSession_Id int    `json:"im_sub_session_id"` //是	会话ID
	OptionId        int    `json:"option_id"`         //是	评价选项ID (2：满意 3：一般 4：不满意)
	Remark          string `json:"remark"`            //是	评价备注
	RobotId         int    `json:"robot_id"`          // 是	udesk机器人id（在系统中 即时通讯->IM机器人 模块获取）
	SceneId         int    `json:"scene_id"`          //是	udesk机器人对应场景id
}

/**
 * @Description: urobot会话评价响应
 */
type URobotSessionAppraiseResp struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

////////////////////////////////////////
/**
 * @Description: urobot回答评价选项
 */
type URobotAnswerAppraiseOption struct {
	CustomerToken  string `json:"customer_token"`    //是	应用端客户唯一标识
	MessageLogId   int    `json:"messgae_id"`        //是	问题答案所在的消息id
	ImSubSessionId int    `json:"im_sub_session_id"` //是	会话id
	OptionId       int    `json:"option_id"`         //是	评价选项id(1表示满意，2表示不满意,默认为满意)
	RobotId        int    `json:"robot_id"`          //是	udesk机器人id
	SceneId        int    `json:"scene_id"`          //是	udesk机器人对应场景id
}

/**
 * @Description: urobot回答评价响应
 */
type URobotAnswerAppraiseResp struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

/**
 * @Description:
 */
////////////////////////////////////////
/**
 * @Description: robot问题调查选项
 */
type RobotQuestionSurveyOption struct {
	CustomerToken string `json:"customer_token"` //	是	应用端客户唯一标识
	MessageId     string `json:"message_id"`     //	是	所评价机器人答案的消息ID
	QuestionId    int    `json:"question_id"`    //	是	问题ID
	Useful        bool   `json:"useful"`         //	是	问题是否有用
}

/////////////////////////////////////////
/**
 * @Description: urobot列表查询响应
 */
type URobotListResp struct {
	Code       int      `json:"code"`        //执行结果码，1000代表成功
	Message    string   `json:"message"`     //返回结果信息
	URobotList []URobot `json:"urobot_list"` //机器人列表的详细信息
}

/**
 * @Description: urobot信息结构
 */
type URobot struct {
	Id        int     `json:"id"`         //urobot机器人id
	Name      string  `json:"name"`       //机器人名称
	Logo      string  `json:"logo"`       //机器人头像
	SceneList []Scene `json:"scene_list"` //适合的场景信息
	CreatedAt string  `json:"created_at"` //机器人的创建时间
}

/**
 * @Description: 场景消息
 */
type Scene struct {
	Id   int    `json:"id"`   //场景id
	Name string `json:"name"` //场景名称
}

///////////////////////////////////////////

type URobotFlowResp struct {
	Code    int            `json:"code"`    //	执行结果码
	Message string         `json:"message"` //执行结果说明
	Data    URobotFlowData `json:"data"`    //	会话信息

}
type URobotFlowData struct {
	SessionId        int              `json:"sessionId"`        //	会话id.
	LogId            int              `json:"logId"`            // 机器人返回的log的标识id.
	FlowId           int              `json:"flowId"`           //	流程id,返回流程时有效.
	FlowTitle        string           `json:"flowTitle"`        //	流程标题，返回流程时有效.
	FlowContent      string           `json:"flowContent"`      //	流程内容，返回流程时有效.内容中只有带有data-type和data-id属性的html元素是可点击的，并且所有的data-*属性的值都是String类型的
	RestrictionState RestrictionState `json:"restrictionState"` // 交互量限制状态.
}
type RestrictionState struct {
	ExceededFlag int `json:"exceededFlag"` //超限标识【0正常/1已超限】
	UsedCount    int `json:"usedCount"`    //	已使用数量
	TotalCount   int `json:"totalCount"`   //	限制总量
}

///////////////////////////////////
/**
 * @Description: 转人工选项
 */
type TransferAgentTriggerOption struct {
	ImSubSessionId int `json:"im_sub_session_id"` //	是	会话id
	RobotId        int `json:"robot_id"`          //	是	机器人id
	SceneId        int `json:"scene_id"`          //	是	机器人场景id
}

/**
 * @Descrip 转人工响应
 */
type TransferAgentTriggerResp = RespWithCodeAndMessage
