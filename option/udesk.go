// @Author：bishisimo  2021/7/17 16:07
// @Description
package option

type UDeskOption struct {
	Email    string `json:"email"`    //Udesk邮箱
	Password string `json:"password"` //Udesk密码
	Domain   string `json:"domain"`   //服务地址
	Api      string `json:"api"`      //api前缀
}
