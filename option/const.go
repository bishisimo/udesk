package option

const (
	SuccessCode = 1000
	QueueCode   = 2001
	VainCode    = 2002
	//////////////////
	URobotQuestionSatisfied    = 1
	URobotQuestionDissatisfied = 2
	URobotSessionSatisfied     = 2
	URobotSessionGeneral       = 2
	URobotSessionDissatisfied  = 4

	UDeskUrlPrefix    = "udesk.url."
	AgentSessionType  = "agent"
	URobotSessionType = "urobot"
	RobotSessionType  = "robot"
)

/**
 * @Description: udeks客服状态枚举
 */
var RespCode = map[int]string{
	1000:  "success",
	2001:  "队列",
	2002:  "子域名无效",                    //	无	域名输入错误
	2005:  "参数有误",                     //无	密码输入错误
	2015:  "非管理员不可操作",                 //无	鉴权使用的身份非管理员/没有找到该资源	无	账号输入错误或无此账号
	2058:  "您的公司不是专业版",                //无	公司未付费
	2059:  "open api签名不对",             //无	鉴权参数错误
	20621: "时间戳格式不对",                  //同message	必填参数{timestamp}未填写或格式错误
	20622: "时间戳误差不能超过5分钟",             //同message	参数{timestamp}与当前时间差超过5分钟
	20623: "请求仅一次有效, 15分钟内nonce值不能重复", //同message	鉴权参数{nonce}在15分钟内已经使用过
	20624: "open api nonce为空",         //同message	鉴权参数{nonce}未填写或者为空
	2000:  "未知错误",                     ///param is missing or the value is empty: notice_url notice_url参数为空	notice_url参数不能为空
}

/*
错误码	message信息	exception:message信息	描述
2002	子域名无效	无	域名输入错误
2005	参数有误	无	密码输入错误
2015	非管理员不可操作	无	鉴权使用的身份非管理员
没有找到该资源	无	账号输入错误或无此账号
2058	您的公司不是专业版	无	公司未付费
2059	open api签名不对	无	鉴权参数错误
20621	时间戳格式不对	同message	必填参数{timestamp}未填写或格式错误
20622	时间戳误差不能超过5分钟	同message	参数{timestamp}与当前时间差超过5分钟
20623	请求仅一次有效, 15分钟内nonce值不能重复	同message	鉴权参数{nonce}在15分钟内已经使用过
20624	open api nonce为空	同message	鉴权参数{nonce}未填写或者为空
2000	未知错误	param is missing or the value is empty: notice_url notice_url参数为空	notice_url参数不能为空
*/
