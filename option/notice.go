package option

/**
 * @Description: 通知配置选项
 * @Deprecated: 测试没有效果
 */
type NoticeOption struct {
	NoticeUrl `json:"notice_url"`
}

/**
 * @Description: 创建通知设置选项
 * @param organization 公司通知地址
 * @param customer 客户通知地址
 * @return *NoticeOption
 * @Deprecated: 测试没有效果
 */
func NewNoticeOption(organization string, customer string) *NoticeOption {
	return &NoticeOption{
		NoticeUrl: *NewNoticeUrl(organization, customer),
	}
}

/**
 * @Description: 通知设置的url结构体，分为公司与客户
 */
type NoticeUrl struct {
	Organization string `json:"organization"` //导入公司通知地址
	Customer     string `json:"customer"`     //导入客户通知地址
}

/**
 * @Description: 创建通知结构所需地址
 * @param organization
 * @param customer
 * @return *NoticeUrl
 */
func NewNoticeUrl(organization string, customer string) *NoticeUrl {
	return &NoticeUrl{Organization: organization, Customer: customer}
}

/**
 * @Description: 通知响应
 */
type NoticeResp struct {
	Code int `json:"code"`
	NoticeUrl
}
