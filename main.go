package udesk

import (
	"gitee.com/bishisimo/udesk/api"
	_ "gitee.com/bishisimo/udesk/config"
)

type (
	UDesk    = api.UDesk
	UDeskCtl = api.UDeskCtl
)

var (
	NewUDeskCtlByOption = api.NewUDeskCtlByOption
	NewUDeskCtl         = api.NewUDeskCtl
)
